/**
 * Blink like a storm
 *
 * Turns on groups of LED in a way that looks like thunderstorm.
 *
 */
#include "Arduino.h"

#define arr_len( x )  ( sizeof( x ) / sizeof( *x ) )

// FIXME
const int NB_LED_GROUP = 2;
const int NB_LED_BY_GRP = 3;
const int ledgroups[][NB_LED_BY_GRP] = {
  {10, 11, 12},
  {7, 8, 9}
};

void setup()
{
  for (int ledpin = 0; ledpin < NB_LED_GROUP ; ledpin++ ){
    for (int i = 0; i < NB_LED_BY_GRP ; i++ ){
      pinMode(ledgroups[ledpin][i], OUTPUT);
    }
  }
}


void toggle_led_group(int idx, typeof(HIGH) val){
  for (int pin = 0; pin < NB_LED_BY_GRP ; pin++ ){
    digitalWrite(ledgroups[idx][pin], val);
  }
}

void loop()
{
  // turn the LED on (HIGH is the voltage level)
  toggle_led_group(0, HIGH);
  delay(100);
  toggle_led_group(0, LOW);
  delay(100);
  toggle_led_group(0, HIGH);
  delay(200);
  
  // turn the LED off by making the voltage LOW
  toggle_led_group(0, LOW);

   // wait for a second
  delay(1000);
}
